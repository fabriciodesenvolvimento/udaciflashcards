//react
import React, { Component } from "react";
import { TouchableOpacity, Platform } from "react-native";
import { Button } from 'react-native-elements';
//redux
import { connect } from "react-redux";
//components
import CardDeck from "../../components/card-deck";
//style
import styled from 'styled-components/native';
import colors from '../../styles/colors';
import { Ionicons } from '@expo/vector-icons';

const Container = styled.View`
  flex: 1;
  background-color: ${colors.Container.bgColor};
`;
const ContainerButton = styled.View`
  justify-content: space-between;
  padding: 25px;
`;


class Details extends Component {

  static navigationOptions = ({ navigation }) => {
    const { state: { params } } = navigation;
    return {
      title: "Deck",
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack(null)}>
          <Ionicons
            name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
            size={28}
            style={{ marginLeft: 12 }}
            color={colors.HeaderBar.textColor}  />
        </TouchableOpacity>
      )
    };
  };

  render() {
    const { deck, cardCount, navigation } = this.props;
    const canStartQuiz = cardCount > 0;

    return (
      <Container>
        <CardDeck
          title={deck.title}
          cards={cardCount}
          onPress={() => { cardCount > 0 ? this.props.navigation.navigate("Cards", deck.title) : null}}
        />

        <ContainerButton>

          <Button
            outline
            borderRadius={10}
            style={{marginBottom: 15}}
            color={colors.Dark.bgColor}
            title="ADD CARD"
            onPress={() => navigation.navigate("NewCard")}
          />
          {canStartQuiz && (
            <Button
              borderRadius={10}
              backgroundColor={colors.Dark.bgColor}
              title="START QUIZ"
              onPress={() => navigation.navigate("Quiz")}
            />
          )}
        </ContainerButton>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  const deck = state.decks.decks[state.decks.deckKeySelected];
  return {
    deck,
    cardCount: Object.keys(deck.questions || {}).length
  };
};

export default connect(mapStateToProps)(Details);
