//react
import React, { Component } from "react";
import { ActivityIndicator, FlatList, Platform, TouchableOpacity } from "react-native";
import Swipeout from "react-native-swipeout";
//redux
import { connect } from 'react-redux';
import DecksAction from 'store/ducks/decks';
//compoenents
import CardDeck from "../../components/card-deck";
//style
import colors from '../../styles/colors';
import styled from 'styled-components/native';
import { Ionicons } from '@expo/vector-icons';
//fab
import FAB from 'react-native-fab'


const Container = styled.View`
  flex: 1;
  background-color: ${colors.Container.bgColor};
`;

class Home extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: "Decks",
    headerRight: Platform.OS === "ios" && (
      <TouchableOpacity onPress={() => navigation.navigate("NewDeck")}>
        <Ionicons
          name="md-add"
          size={28}
          style={{ marginRight: 12 }}
          color={colors.HeaderBar.textColor}  />
      </TouchableOpacity>
    )
  });

  state = {
    titleWidth: 0,
    refreshing: true
  };

  async componentDidMount() {
    await this.props.getAll();
  }

  onRefresh = () => {
    if (this.state.refreshing) {
      this.setState({ refreshing: false });
    }

    this.props.getAll();
  };

  selectedDeck = deck => {
    this.props.selectedDeck(deck.key);
    this.props.navigation.navigate("Details", deck.title);
  };

  deleteDeck = deck => {
    this.props.deleteDeck(deck.key);
  };

  editDeck = deck => {
    this.props.navigation.navigate("NewDeck", deck);
  };

  renderRow = ({ item }) => {
    let swipeBtns = [
      {
        text: "Delete",
        type: "delete",
        onPress: () => this.deleteDeck(item)
      },
      {
        text: "Edit",
        type: "default",
        onPress: () => this.editDeck(item)
      }
    ];

    const { title, questions } = item;

    return (
      <Swipeout
        autoClose={true}
        backgroundColor={colors.Container.bgColor}
        right={swipeBtns}
      >
        <CardDeck
          title={title}
          cards={questions ? Object.keys(questions).length : 0}
          onPress={() => this.selectedDeck(item)}
        />
      </Swipeout>
    );
  };

  render() {
    const { navigation } = this.props;
    const { decks, loading, error } = this.props.decks;
    const arrayDecks = decks ? Object.values(decks) : [];
    const { refreshing } = this.state;

    return (
      <Container>
        {loading && refreshing && (
            <ActivityIndicator
              style={{ marginTop: 50 }}
              size={1}
              color={colors.Primary.darkColor}
            />
          )}

        <FlatList
          data={arrayDecks}
          keyExtractor={item => item.key}
          renderItem={this.renderRow.bind(this)}
          refreshing={loading}
          onRefresh={this.onRefresh}
          onScroll={this.onScroll}
          scrollEventThrottle={20}
        />

        {Platform.OS === "android" && (
          <FAB
            buttonColor={colors.Dark.bgColor}
            iconTextColor={colors.Dark.textColor}
            onClickAction={() => navigation.navigate("NewDeck")}
            visible={true}
            iconTextComponent={<Ionicons name="md-add" />} />
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  decks: state.decks
});

const mapDispatchToProps = {
  getAll: DecksAction.requestDecks,
  selectedDeck: DecksAction.selectedDeck,
  deleteDeck: DecksAction.deleteDeck
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
