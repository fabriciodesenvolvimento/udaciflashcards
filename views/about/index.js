//react
import React from "react";
import { Text, View, Image, ScrollView, Platform } from "react-native";
import { Card, ListItem, Avatar } from 'react-native-elements'
//style
import styled from "styled-components";

const about =
  "This app allows users to study a collection of flashcards. With the app, " +
  "users will be able to create different categories of flashcards called 'decks', " +
  "add flashcards to those decks, and make the quizes in these decks.";

const StyScrollView = styled.ScrollView`
  flex: 1;
`;

const Container = styled.View`
  flex: 1;
  align-items: center;
  padding:10px 20px 0 20px;
`;

const Title = styled.Text`
  font-size: 32px;
  color: #2d3c4b;
  font-weight: 900;
  margin-bottom: 20px;
  text-align: center;
`;

export default class About extends React.Component {
  static navigationOptions = {
    title: "About"
  };
 

  render() {
    
    const list = [
      {
        title: 'Author',
        subtitle: 'Fabricio Farias',
      },
      {
        title: 'Course',
        subtitle: 'Udacity Nanodegrees'
      },
      {
        title: 'version',
        subtitle: '1.0'
      },
    ]

    return (
      <StyScrollView>
        <Container>
          <Avatar
            xlarge
            rounded
            source={require("../../../assets/icon.png")}
            activeOpacity={0.7}
          />
          <Title>Flahscards</Title>

          <Card>
            <Text style={{ marginBottom: 10, textAlign: "center" }}>{about}</Text>
            {list.map((item, i) => (
              <ListItem
                titleStyle={{textAlign: "center"}}
                subtitleStyle={{textAlign: "center"}}
                key={i}
                title={item.title}
                subtitle={item.subtitle}
                chevronColor="white"
              />
              ))
            }
          </Card>
          
        </Container>
      </StyScrollView>
    );
  }
}
