//react
import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { TouchableOpacity, Keyboard, TouchableWithoutFeedback, Platform, Alert } from "react-native";
import { Button, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
//components
import Card from "../../components/card";
//redux
import { connect } from "react-redux";
import DecksAction from "store/ducks/decks";
//style
import styled from 'styled-components/native';
import colors from '../../styles/colors';
import { Ionicons } from '@expo/vector-icons';

const Container = styled.View`
  flex: 1;
  background-color: ${colors.Container.bgColor};
  align-items: center;
`;

class NewDeck extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params } } = navigation;
    return {
      title: params ? "Edit Deck" : "New Deck",
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack(null)}>
          <Ionicons
            name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
            size={28}
            style={{ marginLeft: 12 }}
            color={colors.HeaderBar.textColor}  />
        </TouchableOpacity>
      )
    };
  };

  state = {
    title: "",
    messageValidation: null,
    key: null
  };

  componentDidMount() {
    const { state: { params } } = this.props.navigation;
    if (params) {
      this.setState({ title: params.title, key: params.key });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { success, error } = nextProps.decksState;
    if (success) {
      const resetAction = NavigationActions.reset({
        index: 1,
        actions: [
          NavigationActions.navigate({ routeName: "Home" }),
          NavigationActions.navigate({
            routeName: "Details",
            params: this.state.title
          })
        ]
      });

      this.props.navigation.dispatch(resetAction);
    }
    if (error) {
      this.dropdown.alertWithType("error", "Error", error.toString());
      Alert.alert(
        'Error',
        error.toString()
      )
    }
  }

  saveDeck = () => {

    if (!this.valideForm()) return;

    Keyboard.dismiss();

    const deck = {
      key: this.state.key,
      title: this.state.title
    };

    this.props.saveDeck(deck);
  };

  valideForm = () => {
    this.setState({ messageValidation: null });
    if (this.state.title === "") {
      this.setState({ messageValidation: "This Field is Requerid!" });
      return false;
    }

    return true;
  };

  render() {
    const { title, messageValidation } = this.state;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Container>
          <Card style={{ paddingVertical: 16 }}>
            
            <FormLabel>WHAT IS THE TITLE OF YOUR NEW DECK?</FormLabel>
            <FormInput
              value={title}
              onChangeText={title => this.setState({ title })}
            />
            {messageValidation && <FormValidationMessage>{messageValidation}</FormValidationMessage>}
            

            <Button
              title="SAVE"
              style={{marginTop: 15}}
              backgroundColor={colors.Dark.bgColor}
              small
              borderRadius={10}
              onPress={this.saveDeck}
            />
          </Card>

        </Container>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => ({
  decksState: state.decks
});

const mapDispatchToProps = {
  saveDeck: DecksAction.saveDeck
};

export default connect(mapStateToProps, mapDispatchToProps)(NewDeck);
