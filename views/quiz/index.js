//react
import React, { Component } from "react";
import { View, Text, TouchableOpacity, ScrollView, Platform } from "react-native";
import FlipCard from 'react-native-flip-card'
import { Button, Badge } from 'react-native-elements';
//notification
import { clearLocalNotification, setLocalNotification } from "helpers/notification";
//components
import Card from "../../components/card";
import Result from "../../components/quiz-result";
import Notification from "../../components/quiz-notification";
//redux
import { connect } from "react-redux";
//styled
import { Ionicons } from "@expo/vector-icons";
import styled from "styled-components";
import colors from "../../styles/colors";


const Container = styled.ScrollView`
  flex: 1;
  background-color: ${colors.Container.bgColor};
`;

const StyCard = styled(Card)`
  padding: 20px 0;
  background-color: ${colors.Card.bgColor};
`;

const LabelTextAnswerOrQuestion = styled.Text`
  margin-bottom: 40px;
  margin-top: 30px;
  font-size: 30px;
  text-align: center;
  font-weight: 900;
  color: ${colors.Card.title}
`;

const TextBtnShowAnswerOrQuestion = styled.Text`
  text-align: center;
  font-weight: 900;
  color: ${colors.Secondary.lightColor};
  font-size: 18px;
`;

const ContainerButton = styled.View`
  justify-content: space-between;
  padding: 25px;
`;

const ContainerButtonRow = ContainerButton.extend`
  flex-direction: row;
`;

const StyCardBody = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const LabelCount = styled.Text`
  font-weight: 900;
  color: ${colors.Card.title};
  padding: 8px;
`;

const INITIAL_STATE = {
  cardNumber: 1,
  cardCount: 0,
  correct: 0,
  incorrect: 0,
  showAnswer: false,
  finished: false
};

class Quiz extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Quiz",
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack(null)}>
          <Ionicons
            name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
            size={28}
            style={{ marginLeft: 12 }}
            color={colors.HeaderBar.textColor}  />
        </TouchableOpacity>
      )
    };
  };

  state = INITIAL_STATE;

  componentDidMount() {
    /* receive number of cards */
    const cardCount = Object.keys(this.props.deck.questions || {}).length;
    this.setState({ cardCount });
  }

  showAnswerClick = () => {
    /* show answer */
    const showAnswer = !this.state.showAnswer;
    this.setState({ showAnswer });

    this.flip._toggleCard();
  };

  btnAnswerClick = isCorrect => {

    var { cardNumber, correct, incorrect, cardCount } = this.state;

    if (isCorrect) correct++;
    else incorrect++;

    /* check if is the last card (quiz completed) */
    if (cardCount === cardNumber) {
      this.setState({ finished: true, correct, incorrect, showAnswer: false });
      /* clear notification */
      clearLocalNotification().then(setLocalNotification);

    } else {
      cardNumber++;

      this.setState({ cardNumber, correct, incorrect, showAnswer: false });
    }
  };

  restartQuiz = () => {
    const { cardCount } = this.state;
    this.setState({ ...INITIAL_STATE, cardCount });
  };

  render() {
    const { deck, navigation } = this.props;
    const {
      cardCount,
      cardNumber,
      showAnswer,
      finished,
      correct,
      incorrect
    } = this.state;

    const accuracy = `${(correct * 100 / cardCount).toFixed(2)}%`;
    const questions = Object.values(deck.questions);
    const question = questions[cardNumber - 1];

    return !finished ? (
      <Container>
        <StyCard>
          <StyCardBody>
            <LabelCount> {`${cardNumber}/${cardCount}`} </LabelCount>
            <Badge containerStyle={{ backgroundColor: colors.Card.title }}>
              <Text style={{ color: 'orange' }}>{deck.title}</Text>
            </Badge>
          </StyCardBody>

          <FlipCard
            style={{ borderWidth: 0 }}
            friction={6}
            perspective={1000}
            flipHorizontal={true}
            flipVertical={false}
            flip={false}
            clickable={false}
            ref={flip => this.flip = flip}>
            <LabelTextAnswerOrQuestion> {question.question} </LabelTextAnswerOrQuestion>
            <LabelTextAnswerOrQuestion> {question.answer} </LabelTextAnswerOrQuestion>
          </FlipCard>

          <TouchableOpacity onPress={this.showAnswerClick}>
            <TextBtnShowAnswerOrQuestion> {showAnswer ? "Question" : "Answer"} </TextBtnShowAnswerOrQuestion>
          </TouchableOpacity>
        </StyCard>

        {showAnswer && (
          <ContainerButton>
            <Button
              large
              borderRadius={10}
              backgroundColor={colors.Success.darkColor}
              title="Correct"
              style={{marginBottom: 15}}
              onPress={() => this.btnAnswerClick(true)} />
            <Button
              large
              borderRadius={10}
              backgroundColor={colors.Secondary.darkColor}
              title="Incorret"
              onPress={() => this.btnAnswerClick(false)} />
          </ContainerButton>
        )}
      </Container>
    ) : (
      <View>
      
        <Notification success={correct === cardCount} />
      
        <Result correct={correct} incorrect={incorrect} accuracy={accuracy} />
      
        <ContainerButtonRow>
          <Button
            borderRadius={10}
            title="Restart Quiz"
            backgroundColor={colors.Dark.bgColor}
            onPress={this.restartQuiz} />
          <Button
            borderRadius={10}
            title="Back to Deck"
            backgroundColor={colors.Dark.bgColor}
            onPress={() => navigation.goBack(null)} />
        </ContainerButtonRow>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  deck: state.decks.decks[state.decks.deckKeySelected]
});

export default connect(mapStateToProps)(Quiz);
