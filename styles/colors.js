export default {
  Card: {
    bgColor: "#FFF",
    title: "#2d3c4b",
    subtitle: "#7c8a98"
  },
  Container: {
    bgColor: "#F2F2F2",
  },
  Tab: {
    bgColor: "#fff",
    textColor: "#2d3c4b",
    activeColor: "#2d3c4b",
    inactiveColor: "#7c8a98"
  },
  HeaderBar: {
    bgColor: "#2d3c4b",
    textColor: "#fff",
  },
  Primary: {
    bgColor: "#FFF",
    lightColor: "#2196f3",
    darkColor: "#0080d1",
    textColor: "#FFF",
  },
  Secondary: {
    bgColor: "#fff",
    lightColor: "#e14e60",
    darkColor: "#c92b07",
    textColor: "#fff"
  },
  Success: {
    bgColor: "#fff",
    lightColor: "#39d687",
    darkColor: "#258300",
    textColor: "#fff"
  },
  Dark: {
    bgColor: "#2d3c4b",
    textColor: "#fff",
  }  
};
