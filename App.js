import React from 'react';
import App from "./src";
import { StatusBar } from 'react-native';

// it change the status bar style
StatusBar.setBarStyle("light-content")

export default App