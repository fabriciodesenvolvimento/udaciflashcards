# Projeto Flashcard do curso udacity

Este é um aplicativo mobile (Android e iOS) que permite que você estude uma coleção de flashcards. 
Com o app, você pode criar diferentes categorias de flashcards chamadas de "baralhos", 
adicionar flashcards a esses baralhos, e fazer os quizes nestes baralhos.

## Screen Application

## Android
![alt text](https://i.imgur.com/EYJfmjd.png)

## Ios
![alt text](https://i.imgur.com/buOlxMC.png)

## Decks
![alt text](https://i.imgur.com/jVdkiii.png)

## Deck
![alt text](https://i.imgur.com/dzdXYei.png)

## Form
![alt text](https://i.imgur.com/1eXD8i6.png)

## Quiz
![alt text](https://i.imgur.com/7PTGTjS.png)


# Instalação e Execução

### Instalando 

`yarn install` ou `npm install`

### Executando

`yarn start` ou `npm start`

### Para executar no emulador execute os seguintes comandos

`yarn run ios`ou `yarn run android`

# Bibliotecas usadas neste projeto

* Apisauce - (https://github.com/infinitered/apisauce)
* Redux - (https://redux.js.org/)
* Redux Saga - (https://github.com/redux-saga/redux-saga)
* Reduxsauce - (https://github.com/infinitered/reduxsauce)
* React Navigation - (https://reactnavigation.org/)
* react-native-dropdownalert - (https://github.com/testshallpass/react-native-dropdownalert)
* react-native-flip-card - 
* Firebase - (https://firebase.google.com/)

