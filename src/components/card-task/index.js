//react
import React from "react";
import { Text, StyleSheet } from "react-native";
import { Divider } from "react-native-elements";
//style
import styled from 'styled-components/native';
import colors from '../../styles/colors';

const Container = styled.TouchableOpacity`
  flex-direction: row;
  padding: 6px 0;
  elevation: 5;
  background-color: ${colors.Card.bgColor};
  border-top-width: 1px;
  border-bottom-width: 1px;
  border-style: solid;
  border-color: #dedede;
  margin-bottom: -1px;
`;

const ContainerQ = styled.View`
  padding: 0 15px;
`;

const ContainerA = styled.View`
  padding: 0 15px;
`;

const TextTitle = styled.Text`
  font-size: 12;
  color: ${colors.Card.subtitle};
  padding-bottom: 15px;
`;

const Row = styled.View`
  flex: 1;
`;

const style = StyleSheet.create({
  Divider: {
    marginVertical: 5,
    marginLeft: 15,
    backgroundColor: "#dedede"
  },
  Text: {
    paddingLeft: 15
  }
})
export default CardRow = ({ question, answer, onPress }) => (
  <Container onPress={onPress} >

    <Row>

      <ContainerQ>
        <TextTitle> Question: </TextTitle>
        <Text style={style.Text}> {question} </Text>
      </ContainerQ>
      <Divider style={style.Divider}/>
      <ContainerA>
        <TextTitle> Answer: </TextTitle>
        <Text style={style.Text}>{answer}</Text>
      </ContainerA>
    </Row>
  </Container>
);