import * as firebase from "firebase";

var config = {
  apiKey: "AIzaSyAGtWROqK2YeDOc-FhVIvSdmoyI3KTGdxg",
  authDomain: "udaciflashcards.firebaseapp.com",
  databaseURL: "https://udaciflashcards.firebaseio.com",
  projectId: "udaciflashcards",
  storageBucket: "udaciflashcards.appspot.com",
  messagingSenderId: "1062849171494"
};

firebase.initializeApp(config);

export default firebase.database();