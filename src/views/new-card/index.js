//react
import React, { Component } from "react";
import { Keyboard, TouchableWithoutFeedback, TouchableOpacity, Platform } from "react-native";
import { Button, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
//components
import Card from "../../components/card";
//icons
import { Ionicons } from '@expo/vector-icons';
//redux
import { connect } from "react-redux";
import DecksAction from "store/ducks/decks";
//styled
import styled from 'styled-components/native';
import colors from '../../styles/colors';

const INITIAL_STATE = {
  key: null,
  question: "",
  answer: "",
  questionValidation: null,
  answerValidation: null
};

const Container = styled.View`
  flex: 1;
  background-color: ${colors.Container.bgColor};
  align-items: center;
 `;

class NewCard extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params }} = navigation;
    return ({
      title: params ? "Edit Card" : "New Card",
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack(null)}>
          <Ionicons
            name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
            size={28}
            style={{ marginLeft: 12 }}
            color={colors.Primary.textColor}  />
        </TouchableOpacity>
      )
    });
  }


  state = INITIAL_STATE;

  componentDidMount() {
    const { state: { params }} = this.props.navigation;
    if (params) {
      this.setState({ key: params.key, question: params.question, answer: params.answer });
    }
  }


  componentWillReceiveProps(nextProps) {
    const { success, error } = nextProps.decksState;

    if (success) {
      if (this.state.key) {
        this.props.navigation.goBack(null);
      } else {
        this.setState(INITIAL_STATE);
      }

    }
  }

  saveCard = () => {

    if (!this.valideForm()) return;

    Keyboard.dismiss();

    const { question, answer, key } = this.state;
    const { deck } = this.props;

    this.props.saveCard(deck.key, { question, answer, key });
    this.props.navigation.goBack(null);

  };

  valideForm = () => {
    this.setState({ questionValidation: null, answerValidation: null });
    var isValide = true;

    if (this.state.question === "") {
      this.setState({ questionValidation: "This Field is Requerid!"});
      isValide = false;
    }

    if (this.state.answer === "") {
      this.setState({ answerValidation: "This Field is Requerid!"});
      isValide = false;
    }

    return isValide;
  }

  render() {
    const { question, answer, questionValidation, answerValidation } = this.state;
    
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Container>
          <Card style={{ paddingVertical: 16 }}>
          
            <FormLabel>QUESTION</FormLabel>
            <FormInput
              value={question}
              onChangeText={question => this.setState({ question })}
            />
            {questionValidation && <FormValidationMessage>{questionValidation}</FormValidationMessage>}
            
            <FormLabel style={{marginTop: 15}}>ANSWER</FormLabel>
            <FormInput
              value={answer}
              onChangeText={answer => this.setState({ answer })}
            />
            {answerValidation && <FormValidationMessage>{answerValidation}</FormValidationMessage>}
            
            <Button
              title="SAVE"
              style={{marginTop: 15}}
              backgroundColor={colors.Dark.bgColor}
              size="small"
              borderRadius={10}
              onPress={this.saveCard}
            />
          </Card>
        </Container>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = state => ({
  decksState: state.decks,
  deck: state.decks.decks[state.decks.deckKeySelected]
});

const mapDispatchToProps = {
  saveCard: DecksAction.saveCard
};

export default connect(mapStateToProps, mapDispatchToProps)(NewCard);
