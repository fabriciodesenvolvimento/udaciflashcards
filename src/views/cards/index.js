//react
import React, { Component } from "react";
import { FlatList, TouchableOpacity, Platform, StyleSheet } from "react-native";
import Swipeout from "react-native-swipeout";
//components
import CardTask from "../../components/card-task";
//redux
import { connect } from "react-redux";
import DecksAction from "store/ducks/decks";
//styles
import styled from 'styled-components/native';
import colors from '../../styles/colors';
import { Ionicons } from '@expo/vector-icons';

const Container = styled.View`
  flex: 1;
  background-color: ${colors.Container.bgColor};
`;

class Cards extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params }} = navigation;
    return {
      title: params,
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack(null)}>
          <Ionicons
            name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
            size={28}
            style={{ marginLeft: 12 }}
            color={colors.HeaderBar.textColor}  />
        </TouchableOpacity>
      )
    };
  };

  deleteDeck = card => {
    this.props.deleteCard(this.props.decksState.deckKeySelected, card.key);
  };

  editDeck = card => {
    this.props.navigation.navigate("NewCard", card);
  };

  renderRow = ({ item }) => {
    let swipeBtns = [
      {
        text: "Delete",
        type: "delete",
        onPress: () => this.deleteDeck(item)
      },
      {
        text: "Edit",
        type: "default",
        onPress: () => this.editDeck(item)
      }
    ];

    const { question, answer } = item;

    return (
      <Swipeout
        autoClose={true}
        backgroundColor={colors.Container.bgColor}
        right={swipeBtns}>
        <CardTask
          question={question}
          answer={answer}
          onPress={() => this.props.navigation.navigate("NewCard", item)}
        />
      </Swipeout>
    );
  };

  render() {
    const { deck } = this.props;
    const questions = Object.values(deck.questions || {});

    return (
      <Container>
        <FlatList
          data={questions}
          keyExtractor={item => item.key}
          renderItem={this.renderRow.bind(this)}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  deck: state.decks.decks[state.decks.deckKeySelected],
  decksState: state.decks
});

const mapDispatchToProps = {
  deleteCard: DecksAction.deleteCard
};

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
