//react
import React from "react";
import { TabNavigator, StackNavigator } from "react-navigation";
import { Platform } from "react-native";

//style
import { MaterialCommunityIcons, Ionicons } from "@expo/vector-icons";
import colors from "styles/colors";

/* Views */
import Home from "views/home";
import About from "views/about";
import Details from "views/details";
import NewDeck from "views/new-deck";
import NewCard from "views/new-card";
import Quiz from "views/quiz";
import Cards from "views/cards";


const HomeNavigator = StackNavigator(
  {
    Home: { screen: Home },
    Details: { screen: Details },
    NewDeck: { screen: NewDeck },
    NewCard: { screen: NewCard },
    Cards: { screen: Cards }
  },
  {
    headerMode: "none"
  }
);

/* Tab Navigator */
const Tab = TabNavigator(
  {
    Home: {
      screen: HomeNavigator,
      navigationOptions: {
        tabBarIcon: () => <MaterialCommunityIcons name="cards-variant" size={30} color={colors.Tab.inactiveColor} />
      }
    },
    About: {
      screen: About,
      navigationOptions: {
        tabBarIcon: () => <Ionicons name={Platform.OS === "ios" ? "ios-information-circle" : "md-information-circle" } size={30} color={colors.Tab.inactiveColor} />
      }
    }
  },
  {
    swipeEnabled: false,
    animationEnabled: true,
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      activeTintColor: colors.Tab.activeColor,
      inactiveTintColor: colors.Tab.inactiveColor,
      style: {
        backgroundColor: colors.Tab.bgColor
      },
      labelStyle: {
        fontWeight: "900"
      }
    }
  }
);

/* Stack Root Navigator */
const RootNavigator = StackNavigator(
  {
    Root: { screen: Tab },
    Quiz: { screen: Quiz }
  },
  {
    mode: "modal",
    navigationOptions: {
      headerStyle: { backgroundColor: colors.HeaderBar.bgColor, elevation: null },
      headerTitleStyle: { color: colors.HeaderBar.textColor }
    }
  }
);

export default RootNavigator;
