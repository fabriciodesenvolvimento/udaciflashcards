//react
import React from "react";
import { View, Text } from "react-native";
//icons
import { Ionicons } from "@expo/vector-icons";
//styled 
import styled from "styled-components";
import colors from "../../styles/colors";

const Container = styled.View`
  flex-direction: row;
  margin: 12px 8px 0 8px;
  padding: 26px 0;
  border-radius: 2;
`;
const StyText = styled.Text`
  color: #fff;
  font-weight: 900;
  font-size: 14px;
`;

const Icon = styled.View`
  padding:0 18px;
`;
const Body = styled.View`
`;

export default Notification = ({ success }) => (
  <Container style={{ backgroundColor: success ? colors.Success.lightColor : colors.Secondary.lightColor }} >
    <Icon>
      <Ionicons size={28} name={success ? "md-happy" : "md-sad"} color="#fff" />
    </Icon>
    <View>
      <StyText>{success ? "Well done!" : "Oh no!"} </StyText>
      <StyText>
        {success
          ? "You successfully finished this deck."
          : "You need to practice more this deck."}
      </StyText>
    </View>
  </Container>
);