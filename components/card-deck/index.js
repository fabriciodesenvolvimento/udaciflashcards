//react
import React from "react";
//component
import Card from "../card";
//style
import colors from "styles/colors";
import styled from 'styled-components/native';

const Container = styled.TouchableOpacity`
  padding: 50px;
  align-items: center;
  justify-content: center;
`;

const Title = styled.Text`
  font-weight: 900;
  font-size: 22px;
  color: ${colors.Card.title};
`;

const Subtitle = styled.Text`
  font-weight: 900;
  font-size: 18px;
  color: ${colors.Card.subtitle};
`;



export default CardDeck = ({ onPress, title, cards }) => {
  return (
    <Card>
      <Container onPress={onPress} disabled={onPress ? false : true} >
        <Title>{title}</Title>
        <Subtitle>{cards} cards</Subtitle>
      </Container>
    </Card>
  );
}



