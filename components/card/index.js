//react
import React from "react";
//style
import styled from 'styled-components/native';
import colors from '../../styles/colors';

const Container = styled.View`
  border-top-width: 1px;
  border-bottom-width: 1px;
  border-style: solid;
  border-color: #ccc;
  border-radius: 2px;
  background-color: ${colors.Card.bgColor};
  flex-direction: row;
  margin-bottom: -1px;
  elevation: 2;
`;

const Children = styled.View`
    flex: 1;
    padding: 25px;
`;

export default Card = ({ children }) => {

  return (
    <Container>
      <Children>
        {children}
      </Children>
    </Container>
  );
};