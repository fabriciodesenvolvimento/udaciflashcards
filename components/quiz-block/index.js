//react
import React from "react";
import { View, Text } from "react-native";
//styled
import styled from "styled-components";
import colors from "../../styles/colors";

const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const StyText = styled.Text`
  color: ${colors.Card.subtitle};
  font-weight: 900;
  font-size: 12px;
  padding-bottom: 6px;
`;

const StyValue = styled.Text`
  color: ${colors.Card.subtitle};
  font-weight: 900
`;

export default Block = ({ text, value, border }) => (
  <Container
    style={
      border
        ? { borderLeftWidth: 1, borderRightWidth: 1, borderColor: "#dde0e4" }
        : {}
    }
  >
    <StyText>{text}</StyText>
    <StyValue>{value}</StyValue>
  </Container>
);