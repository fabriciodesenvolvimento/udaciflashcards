//react
import React from "react";
import { View } from "react-native";
//component
import Block from "../quiz-block";
//styles
import styled from 'styled-components';


const Container = styled.View`
  flex-direction: row;
  background-color: #fff;
  margin: 12px 8px 0 8px;
  margin-top: 12px;
  padding: 26px 0;
  border-radius: 2px;
`;

export default Result = ({ correct, incorrect, accuracy }) => (
  <Container>
    <Block text="CORRECT" value={correct} />
    <Block text="INCORRECT" value={incorrect} border />
    <Block text="ACCURACY" value={accuracy} />
  </Container>
);